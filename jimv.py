#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
pip install libvirt-python xmltodict paramiko jimit
"""


import libvirt
import sys
import xmltodict
import os
import json
import paramiko
import jimit as ji
from jimit.terminal.color import Color as tc


__author__ = 'James Iter'
__date__ = '16/6/24'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2016 by James Iter.'


class PathExist(Exception):
    pass


class PathNotExist(Exception):
    pass


class ConnFailed(Exception):
    pass


class AlreadyUsed(Exception):
    pass


class DomainNotExist(Exception):
    pass


class JimV(object):
    def __init__(self, **kwargs):
        self.config_path = kwargs.get('config_path', '/etc/jimv.json')
        self.config = dict()
        self.vmhost = VMHost()
        self.vmguest = VMGuest()
        self.name_prefix = ''
        self.vnc_start_port = 6001
        self.vnc_password_policy = 6001
        self.quantity = 1

    def load_config(self):
        if not os.path.isfile(self.config_path):
            raise PathNotExist(u'配置文件不存在, 请配置 --> ', self.config_path)

        with open(self.config_path, 'r') as f:
            self.config = json.load(f)

    def init_base_info(self):
        self.vmhost.template_pool = self.config['template_pool']

    def show_vmhost(self):
        print '\t'.join(['Seq', 'VMHost'])
        for i, vmhost in enumerate(self.config['vmhost']):
            print '\t'.join([i.__str__(), vmhost])

    def show_container(self):
        print '\t'.join(['Seq', 'Container'])
        for i, container in enumerate(self.config['container']):
            print '\t'.join([i.__str__(), json.dumps(container, sort_keys=True, separators=('\t\t', '-> '))])

    def destroy(self):
        if self.vmguest.name not in self.vmhost.domains:
            raise DomainNotExist(u'欲销毁的虚拟机不存在 --> ' + self.vmguest.name)
        input_str = ji.Common.raw_input(tc.warning_blink('警告: ') + u'确定删除 --> ' + tc.warning_blink(self.vmguest.name) +
                                        '[y/N]', echo=True).upper()
        if input_str != 'Y':
            return u'取消删除操作!'
        domain = self.vmhost.conn.lookupByName(self.vmguest.name)
        domain.undefine()
        return domain.destroy()

    def make_system_image(self):
        if self.vmguest.clone:
            guest_name = ''.join(['L_', self.vmguest.name, '.qcow2'])
        else:
            guest_name = ''.join(['F_', self.vmguest.name, '.qcow2'])

        system_image_base_path = '/'.join([self.config['disk_pool'], self.vmguest.name])
        self.vmguest.system_image_path = '/'.join([system_image_base_path, guest_name])

        if self.vmhost.file_exist(self.vmguest.system_image_path):
            raise PathExist(u'系统镜像已存在 --> ' + self.vmguest.system_image_path)

        self.vmhost.ssh_client.exec_command(' '.join(['mkdir', '-p', system_image_base_path]))

        if self.vmguest.clone:
            if self.config['debug']:
                cmd = ' '.join([self.config['qemu_img'], 'create', '-f', 'qcow2', '-b', self.vmguest.templates_path,
                                self.vmguest.system_image_path])
            else:
                cmd = ' '.join([self.config['qemu_img'], 'create', '-q', '-f', 'qcow2', '-b', self.vmguest.templates_path,
                                self.vmguest.system_image_path])
            stdin, stdout, stderr = self.vmhost.ssh_client.exec_command(cmd)
        else:
            stdin, stdout, stderr = self.vmhost.ssh_client.exec_command(
                ' '.join(['cp', '-v', self.vmguest.templates_path, self.vmguest.system_image_path])
            )

        self.vmguest.container['domain']['devices']['disk'].append(
            {
                "@type": "file",
                "@device": "disk",
                "driver": {
                    "@name": "qemu",
                    "@type": "qcow2"
                },
                "source": {
                    "@file": self.vmguest.system_image_path
                },
                "target": {
                    "@dev": "sda",
                    "@bus": "virtio"
                }
            }
        )

        for line in stdout:
            print line.strip('\n')

    def make_disk_image(self):
        size = ''.join([self.vmguest.disk_size.__str__(), 'G'])
        disk_name = ''.join(['D_', self.vmguest.name, '.qcow2'])
        self.vmguest.disk_image_path = '/'.join([self.config['disk_pool'], self.vmguest.name, disk_name])

        if self.vmhost.file_exist(self.vmguest.disk_image_path):
            raise PathExist(u'磁盘镜像已存在 --> ' + self.vmguest.disk_image_path)

        if self.config['debug']:
            cmd = ' '.join([self.config['qemu_img'], 'create', '-f', 'qcow2', self.vmguest.disk_image_path, size])
        else:
            cmd = ' '.join([self.config['qemu_img'], 'create', '-q', '-f', 'qcow2', self.vmguest.disk_image_path, size])

        stdin, stdout, stderr = self.vmhost.ssh_client.exec_command(cmd)
        self.vmguest.container['domain']['devices']['disk'].append(
            {
                "@type": "file",
                "@device": "disk",
                "driver": {
                    "@name": "qemu",
                    "@type": "qcow2"
                },
                "source": {
                    "@file": self.vmguest.disk_image_path
                },
                "target": {
                    "@dev": "sdb",
                    "@bus": "virtio"
                }
            }
        )
        for line in stdout:
            print line.strip('\n')

    @staticmethod
    def exit_shell(char=None):
        if char.upper() == 'Q':
            print u'JimV 已正常退出'
            exit(0)

    def choice_vmhost(self):
        while True:
            self.show_vmhost()
            input_str = ji.Common.raw_input(u'连接到: ', echo=True)
            self.exit_shell(char=input_str)

            if not input_str.isdigit() or 0 > int(input_str) or int(input_str) >= self.config['vmhost'].__len__():
                print u'不存在的目标, 请重新选择.'
                continue
            qemu_host = self.config['vmhost'][int(input_str)]
            self.vmhost.conn_to(qemu_host=qemu_host)
            return u'已连接到 --> ' + qemu_host

    def choice_guest_name(self, create=True):
        self.vmhost.get_domains()
        while True:
            if create:
                input_str = ji.Common.raw_input(u'请命名虚拟机实例: ', echo=True)
                self.exit_shell(char=input_str)
                self.vmguest.name = input_str
                if self.vmguest.name in self.vmhost.domains:
                    print u'该实例名已存在, 请重新命名虚拟机实例.'
                    print u'已存在的虚拟机实例: '
                    self.vmhost.show_domains()
                    continue

                return u'虚拟机命名为 --> ' + self.vmguest.name
            else:
                self.vmhost.show_domains()
                input_str = ji.Common.raw_input(u'请选择虚拟机实例: ', echo=True)
                self.exit_shell(char=input_str)
                if not input_str.isdigit() or 0 > int(input_str):
                    print u'不存在的目标, 请重新选择.'
                    continue
                self.vmguest.name = self.vmhost.domains[int(input_str)]
                return u'选择的虚拟机实例为 --> ' + self.vmguest.name

    def choice_container(self):
        while True:
            self.show_container()
            input_str = ji.Common.raw_input(u'选择虚拟机配置: ', echo=True)
            self.exit_shell(char=input_str)
            if not input_str.isdigit() or 0 > int(input_str) or int(input_str) >= self.config['container'].__len__():
                print u'不存在的目标, 请重新选择.'
                continue
            self.vmguest.container = self.config['container'][int(input_str)]
            return u'已选择的虚拟机配置 --> ' + json.dumps(self.vmguest.container)

    def choice_template(self):
        self.vmhost.get_templates()
        while True:
            self.vmhost.show_templates()
            input_str = ji.Common.raw_input(u'选择克隆的模板: ', echo=True)
            self.exit_shell(char=input_str)
            if not input_str.isdigit() or 0 > int(input_str) or int(input_str) >= \
                    self.config['template_pool'].__len__():
                print u'不存在的目标, 请重新选择.'
                continue
            self.vmguest.templates_path = '/'.join([self.vmhost.template_pool, self.vmhost.templates[int(input_str)]])
            return u'已选择的虚拟机模板 --> ' + self.vmhost.templates[int(input_str)]

    def choice_clone_mode(self):
        while True:
            input_str = ji.Common.raw_input(u'选择虚拟机克隆模式(链接克隆L 或 完整克隆F): ', echo=True).upper()
            self.exit_shell(char=input_str)
            if input_str not in ('L', 'F'):
                print u'无效的选择.'
                print u'请选择 --> 链接克隆L 或 完整克隆F'
                continue

            self.vmguest.clone = True
            if input_str == 'F':
                self.vmguest.clone = False
            return u'已选择的虚拟机克隆模式 --> ' + '链接克隆' if self.vmguest.clone else '完整克隆'

    def choice_network(self):
        self.vmhost.get_networks()
        while True:
            self.vmhost.show_networks()
            input_str = ji.Common.raw_input(u'选择VNC侦听的网络: ', echo=True)
            self.exit_shell(char=input_str)
            if not input_str.isdigit() or 0 > int(input_str) or int(input_str) >= self.vmhost.networks.__len__():
                print u'不存在的目标, 请重新选择.'
                continue
            self.vmguest.network = self.vmhost.networks[int(input_str)]
            return u'已选择的VNC侦听网络 --> ' + self.vmguest.network

    def choice_vnc_port(self):
        self.vmhost.get_used_ports()
        while True:
            input_str = ji.Common.raw_input(u'指定VNC侦听的端口: ', echo=True)
            self.exit_shell(char=input_str)
            if not input_str.isdigit() or 1024 > int(input_str) or int(input_str) >= 65535:
                print u'超出有效值范围 1024 ~ 65535.'
                continue

            if input_str in self.vmhost.used_ports:
                print u'指定的端口已被占用, 请重新指定.'
                print u'已被占用的端口: '
                self.vmhost.show_used_ports()
                continue

            self.vmguest.vnc_port = input_str
            return u'已选择的VNC侦听端口 --> ' + self.vmguest.vnc_port

    def choice_bridge(self):
        self.vmhost.get_bridges()
        while True:
            self.vmhost.show_bridges()
            input_str = ji.Common.raw_input(u'选择虚拟机链接的网桥: ', echo=True)
            self.exit_shell(char=input_str)
            if not input_str.isdigit() or 0 > int(input_str) or int(input_str) >= self.vmhost.bridges.__len__():
                print u'不存在的目标, 请重新选择.'
                continue
            self.vmguest.bridge = self.vmhost.bridges[int(input_str)]
            return u'已选择的网桥 --> ' + self.vmguest.bridge

    def choice_disk_size(self):
        while True:
            input_str = ji.Common.raw_input(u'设置虚拟机磁盘大小(单位: GB): ', echo=True)
            self.exit_shell(char=input_str)
            if not input_str.isdigit() or 1 > int(input_str) or int(input_str) >= 10240:
                print u'超出有效值范围 1 ~ 10240'
                continue
            self.vmguest.disk_size = input_str
            return u'已选择的磁盘大小 --> ' + self.vmguest.disk_size

    def choice_vm_quantity(self):
        while True:
            input_str = ji.Common.raw_input(u'欲批量创建虚拟机实例的数量 1 ~ 1024: ', echo=True)
            self.exit_shell(char=input_str)
            if not input_str.isdigit() or 1 > int(input_str) or int(input_str) > 1024:
                print u'超出有效值范围 1 ~ 1024'
                continue
            self.quantity = int(input_str)
            return u'将创建 --> ' + self.quantity.__str__() + u' 虚拟机实例.'

    def choice_name_prefix(self):
        self.vmhost.get_domains()
        while True:
            input_str = ji.Common.raw_input(u'请指定批量虚拟机实例名称前缀: ', echo=True)
            self.exit_shell(char=input_str)
            if input_str.__len__() < 1:
                print u'前缀至少一个字符, 请重新指定.'
                continue

            self.name_prefix = input_str
            try:
                for item in range(self.quantity):
                    name = self.name_prefix + (self.vnc_start_port - 6000 + item).__str__()
                    if name in self.vmhost.domains:
                        print u'批量实例中 --> ' + name + u'已被占用, 请重新指定.'
                        print u'已存在的实例: '
                        self.vmhost.show_domains()
                        raise AlreadyUsed(u'批量实例中 --> ' + name + u'已被占用, 请重新指定.')
            except AlreadyUsed, e:
                continue

            return u'已指定的批量虚拟机实例名称前缀 --> ' + self.name_prefix

    def choice_vnc_start_port(self):
        self.vmhost.get_used_ports()
        while True:
            input_str = ji.Common.raw_input(u'指定VNC起始端口: ', echo=True)
            self.exit_shell(char=input_str)
            if not input_str.isdigit() or 1024 > int(input_str) or int(input_str) >= 65535:
                print u'超出有效值范围 1024 ~ 65535.'
                continue

            self.vnc_start_port = int(input_str)
            try:
                for item in range(self.quantity):
                    port = self.vnc_start_port + item
                    if port.__str__() in self.vmhost.used_ports:
                        print u'批量端口中 --> ' + port.__str__() + u'已被占用, 请重新指定.'
                        print u'已被占用的端口: '
                        self.vmhost.show_used_ports()
                        raise AlreadyUsed(u'批量端口中 --> ' + port.__str__() + u'已被占用, 请重新指定.')
            except AlreadyUsed, e:
                continue

            return u'已选择的VNC起始端口 --> ' + self.vnc_start_port.__str__()

    def choice_password_policy(self):
        while True:
            input_str = ji.Common.raw_input(u'指定VNC密码策略(空密码N 或 随机值R 或 指定密码字符不少于6位): ', echo=True).upper()
            self.exit_shell(char=input_str)
            if input_str not in ('N', 'R') and input_str.__len__() < 6:
                print u'无效的选择.'
                print u'请选择 --> 空密码N 或 随机值R 或 指定密码字符不少于6位'
                continue

            self.vnc_password_policy = input_str
            return u'已选择的定VNC密码策略 --> ' + self.vnc_password_policy

    def create(self):
        if self.config['debug']:
            print u'生成虚拟机 [配置结构] 中...........'
        self.vmguest.generate_container()
        if self.config['debug']:
            print u'虚拟机 [配置结构] 生成完毕.'
            print u'创建虚拟机 [系统镜像] .............'
        self.make_system_image()
        if self.config['debug']:
            print u'虚拟机 [系统镜像] 创建完毕.'
            print u'创建虚拟机 [磁盘镜像] .............'
        self.make_disk_image()
        if self.config['debug']:
            print u'虚拟机 [磁盘镜像] 创建完毕.'
            print u'创建虚拟机 [tap设备] .............'
        self.vmhost.get_taps()
        self.vmguest.generate_tap(self.vmhost.taps)
        if self.config['debug']:
            print u'虚拟机 [tap设备] 创建完毕.'
            print u'创建虚拟机 [网络接口\图形console] .............'
        self.vmguest.padding_interface()

        if self.vnc_password_policy == 'N':
            self.vmguest.vnc_password = ''
        elif self.vnc_password_policy == 'R':
            self.vmguest.vnc_password = ji.Common.generate_random_code(length=8)
        else:
            self.vmguest.vnc_password = self.vnc_password_policy

        if self.config['debug']:
            print u'VNC连接密码: ' + self.vmguest.vnc_password
        self.vmguest.padding_graphics()
        if self.config['debug']:
            print u'虚拟机 [网络接口\图形console] 创建完毕.'
            print u'创建虚拟机 [实例] .............'
        self.vmguest.xml = xmltodict.unparse({'domain': self.vmguest.container['domain']})
        with self.vmhost.sftp.open(
                '/'.join([self.config['disk_pool'], self.vmguest.name, 'origin_define.xml']), 'w', -1) as f:
            f.write(self.vmguest.xml)
        print ''
        print '+' * 80
        print u'实例名称: ' + self.vmguest.container['domain']['name']
        print u'CPU: ' + self.vmguest.container['cpu'].__str__()
        print u'内存: ' + self.vmguest.container['mem'].__str__() + u'GB'
        print u'扩展磁盘大小: ' + self.vmguest.disk_size.__str__() + u'GB'
        print u'网桥: ' + self.vmguest.bridge
        print u'VNC网桥: ' + self.vmguest.network
        print u'VNC端口: ' + self.vmguest.vnc_port.__str__()
        print u'VNC密码: ' + self.vmguest.vnc_password
        print '=' * 80
        if self.vmhost.conn.defineXML(self.vmguest.xml):
            print u'域定义成功.'
        domain = self.vmhost.conn.lookupByName(self.vmguest.name)
        domain.setAutostart(1)
        print u'域设为自动启动.'
        domain.create()
        print u'域成功启动.'
        return domain

    def launch(self):
        self.load_config()
        self.init_base_info()
        print self.choice_vmhost()

        prompt = u'创建虚拟机实例输入 --> [C], 删除虚拟机实例输入 --> d, 批量创建虚拟机实例 --> r: '
        input_str = ji.Common.raw_input(prompt=prompt, echo=True).upper()
        self.exit_shell(char=input_str)
        while input_str not in ('C', 'D', 'R'):
            print u'请输入有效值!'
            input_str = ji.Common.raw_input(prompt=prompt, echo=True).upper()

        if input_str == 'C':
            while True:
                print self.choice_guest_name()
                print self.choice_container()
                print self.choice_clone_mode()
                print self.choice_template()
                print self.choice_network()
                print self.choice_vnc_port()
                print self.choice_password_policy()
                print self.choice_bridge()
                print self.choice_disk_size()
                domain = self.create()
                print u'虚拟机 [实例] ' + domain.name() + u' 创建完毕.'
        elif input_str == 'D':
            while True:
                print self.choice_guest_name(create=False)
                if self.destroy() == 0:
                    print u'虚拟机 [实例] ' + self.vmguest.name + u' 销毁完毕.'
                else:
                    print u'虚拟机 [实例] ' + self.vmguest.name + u' 销毁失败.'
        elif input_str == 'R':
            print self.choice_container()
            print self.choice_clone_mode()
            print self.choice_template()
            print self.choice_network()
            print self.choice_bridge()
            print self.choice_disk_size()

            print self.choice_vm_quantity()
            print self.choice_vnc_start_port()
            print self.choice_password_policy()
            print self.choice_name_prefix()

            for item in range(self.quantity):
                self.vmguest.vnc_port = self.vnc_start_port + item
                self.vmguest.name = self.name_prefix + (self.vnc_start_port - 6000 + item).__str__()
                self.create()


class VMHost(object):
    def __init__(self):
        self.taps = list()
        self.domains = list()
        self.networks = list()
        self.bridges = list()
        self.used_ports = list()
        self.conn = None
        self.template_pool = ''
        self.templates = list()
        self.ssh_client = None
        self.sftp = None
        self.user = 'root'
        self.hostname = ''

    def init_ssh_client(self):
        self.ssh_client = paramiko.SSHClient()
        self.ssh_client.load_system_host_keys()
        self.ssh_client.set_missing_host_key_policy(paramiko.MissingHostKeyPolicy())
        self.ssh_client.connect(hostname=self.hostname, username=self.user)
        self.sftp = self.ssh_client.open_sftp()

    def get_domains(self):
        self.domains = list()
        for item in self.conn.listAllDomains():
            self.domains.append(item.name())

    def show_domains(self):
        for i, domain in enumerate(self.domains):
            print '\t'.join([i.__str__(), domain])

    def get_taps(self):
        self.taps = list()
        for interface in self.conn.listAllInterfaces():
            j_interface = xmltodict.parse(interface.XMLDesc())
            if j_interface['interface']['@type'] == 'bridge' and \
                    isinstance(j_interface['interface']['bridge']['interface'], list):
                for j_ethernet_interface in j_interface['interface']['bridge']['interface']:
                    if j_ethernet_interface['@type'] == 'ethernet':
                        self.taps.append(j_ethernet_interface['@name'])

    def get_templates(self):
        self.templates = list()
        stdin, stdout, stderr = self.ssh_client.exec_command(' '.join(['ls', self.template_pool]))
        for line in stdout:
            self.templates.append(line.strip())

    def show_templates(self):
        for i, template in enumerate(self.templates):
            print '\t'.join([i.__str__(), template])

    def get_networks(self):
        self.networks = list()
        for item in self.conn.listAllNetworks():
            self.networks.append(item.name())

    def show_networks(self):
        for i, network in enumerate(self.networks):
            print '\t'.join([i.__str__(), network])

    def get_bridges(self):
        self.bridges = list()
        stdin, stdout, stderr = self.ssh_client.exec_command("brctl show|grep '^[a-zA-Z]'|sed -n '2,$p'")
        for line in stdout:
            line = line.strip().split('\t')[0]
            self.bridges.append(line)

    def show_bridges(self):
        for i, bridge in enumerate(self.bridges):
            print '\t'.join([i.__str__(), bridge])

    def get_used_ports(self):
        self.used_ports = list()
        stdin, stdout, stderr = self.ssh_client.exec_command(
            "netstat -tnl|grep '^tcp '|awk -F'[:]' '{print $2}'|cut -d' ' -f 1")
        for line in stdout:
            line = line.strip()
            self.used_ports.append(line)

    def show_used_ports(self):
        for i, used_port in enumerate(self.used_ports):
            print '\t'.join([i.__str__(), used_port])

    def file_exist(self, path):
        stdin, stdout, stderr = self.ssh_client.exec_command(
            '[ -e ' + path + ' ]; echo $?')

        for line in stdout:
            if bool(line.strip('\n')):
                # Linux 程序返回值与布尔值相反, Linux中程序正常结束返回0, 否则返回非0. 布尔值0为假, 非0为真;
                return False

        return True

    def conn_to(self, qemu_host=''):
        self.conn = libvirt.open(qemu_host)
        if self.conn is None:
            raise ConnFailed(u'打开连接失败 --> ' + qemu_host + sys.stderr)

        qemu_host = qemu_host.split('/')[2].split('@')
        self.user = qemu_host[0]
        self.hostname = qemu_host[1]
        self.init_ssh_client()


class VMGuest(object):
    def __init__(self):
        self.templates_path = ''
        self.clone = True
        self.system_image_path = ''
        self.disk_image_path = ''
        self.name = ''
        self.bridge = ''
        self.disk_size = '100'
        self.network = ''
        self.tap = ''
        self.vnc_port = ''
        self.container = dict()
        self.xml = ''
        self.vnc_password = ''

    def generate_tap(self, already_exist):
        self.tap = ji.Common.generate_random_code(length=4, letter_form='lower')
        while self.tap in already_exist:
            self.tap = ji.Common.generate_random_code(length=4, letter_form='lower')

    def generate_container(self):
        self.container['domain'] = {
            "@type": "kvm",
            "name": self.name,
            "title": self.name + "_title",
            "description": self.name + "_description",
            "memory": {
                "@unit": "GiB",
                "#text": str(self.container['mem'])
            },
            "features": {
                "acpi": "",
                "apic": ""
            },
            "vcpu": str(self.container['cpu']),
            "os": {
                "type": {
                    "@arch": "x86_64",
                    "#text": "hvm"
                },
                "boot": [
                    {"@dev": "hd"},
                    {"@dev": "cdrom"}
                ],
                "bootmenu": {
                    "@enable": "yes",
                    "@timeout": "3000"
                }
            },
            "devices": {
                "disk": [],
                "interface": []
            },
            "console": {
                "@type": "pty",
                "source": {
                    "@path": "/dev/pts/1"
                },
                "target": {
                    "@type": "virtio",
                    "@port": "0"
                }
            }
        }

    def padding_interface(self):
        self.container['domain']['devices']['interface'].append(
            {
                "@type": "bridge",
                "source": {
                    "@bridge": self.bridge
                },
                "target": {
                    "@dev": ''.join(['tap-', self.tap])
                },
                "model": {
                    "@type": "virtio"
                }
            }
        )

    def padding_graphics(self):
        self.container['domain']['devices']['graphics'] = {
            "@type": "vnc",
            "@port": self.vnc_port.__str__(),
            "@keymap": "en-us",
            "@passwd": self.vnc_password,
            "listen": {
                "@type": "network",
                "@network": self.network
            }
        }

jv = JimV()
jv.launch()
