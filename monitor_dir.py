#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
参考链接:
    https://github.com/seb-m/pyinotify
    https://www.ibm.com/developerworks/cn/linux/l-inotify/

pip install pyinotify
"""


import traceback
import logging
from logging.handlers import TimedRotatingFileHandler
import pyinotify
import signal
import sys

# import pydevd
# pydevd.settrace('your ip', port=51234, stdoutToServer=True, stderrToServer=True)


__author__ = 'James Iter'
__date__ = '2016/10/31'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2016 by James Iter.'


process_title = 'monitor_dir'
pid_file = '/var/run/' + process_title + '.pid'
monitor_dir = '/tmp'


class Init(object):
    @staticmethod
    def init_logger():
        log_file_path = '/var/log/' + process_title + '.log'
        _logger = logging.getLogger(process_title)

        _logger.setLevel(logging.DEBUG)

        fh = TimedRotatingFileHandler(log_file_path, when='W6', interval=1, backupCount=7)
        formatter = logging.Formatter(
            '%(asctime)s - %(name)s - %(levelname)s - %(funcName)s - %(lineno)s - %(message)s')
        fh.setFormatter(formatter)
        _logger.addHandler(fh)
        return _logger


logger = Init.init_logger()


class EventHandler(pyinotify.ProcessEvent):

    def process_IN_CREATE(self, event):
        logger.info(event.pathname + ' Created!')

    def process_IN_DELETE(self, event):
        logger.info(event.pathname + ' Removed!')


wm = pyinotify.WatchManager()
mask = pyinotify.IN_DELETE | pyinotify.IN_CREATE

notifier = pyinotify.Notifier(wm, EventHandler())


def signal_handle(signum=0, frame=None):
    notifier.stop()
    sys.exit(0)


if __name__ == '__main__':
    # noinspection PyBroadException
    try:
        signal.signal(signal.SIGTERM, signal_handle)
        signal.signal(signal.SIGINT, signal_handle)
        wm.add_watch(monitor_dir, mask=mask, rec=True, auto_add=True)
        notifier.loop(daemonize=True, pid_file=pid_file)
    except:
        logger.error(traceback.format_exc())

