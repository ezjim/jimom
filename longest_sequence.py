#!/usr/bin/env python
# -*- coding: utf-8 -*-


__author__ = 'James Iter'
__date__ = '2017/4/1'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2017 by James Iter.'


def longest_sequence(seq):
    # No.1
    # 开始写代码，实现寻找并返回最长递增子序列
    start_point = 0
    arr = []

    for i in range(1, seq.__len__()):
        if seq[i] < seq[i - 1]:
            arr.append(seq[start_point:i])
            start_point = i

        if i + 1 == seq.__len__():
            arr.append(seq[start_point:i + 1])

    max_arr = arr[0]
    for i in range(1, arr.__len__()):
        if max_arr.__len__() < arr[i].__len__():
            max_arr = arr[i]

    return max_arr


def longest_sequence2(seq=None):
    assert isinstance(seq, list)

    before_last_start_point = 0
    before_last_count = 0
    last_start_point = 0

    for i in range(1, seq.__len__()):
        if seq[i] < seq[i - 1] or (i + 1) == seq.__len__():
            if (i - last_start_point) > before_last_count:
                before_last_start_point = last_start_point
                before_last_count = i - last_start_point
                # 最后一个递增元素的范围计算比较特殊
                if (i + 1) == seq.__len__():
                    before_last_count += 1

            last_start_point = i

    return seq[before_last_start_point:before_last_start_point + before_last_count]

# end_code

if __name__ == "__main__":
    seq = [0, -3, 8, 3, 12, 2, 10, 6, 14, 1, 3, 12, 9, 5, 13, 3, 11, 7, 15]
    seq = [1, 2, 3, 4, 1, 2, 3, 5, 6, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    print (longest_sequence(seq))
    print (longest_sequence2(seq))
