#! /usr/bin/python3
# -*- coding: utf-8 -*-

import os
import sys
import re

ser = {
    "host": {
        "can_use_user": ["root"],
        "list":{
            "1": {"name": "Node 01", "ip": "172.16.9.1", "port": "22", "user": "root"},
            "2": {"name": "Node 02", "ip": "172.16.9.2", "port": "22", "user": "root"},
            "3": {"name": "Node 03", "ip": "172.16.9.3", "port": "22", "user": "root"},
        }
    },
    "base": {
        "can_use_user": ["root", "admin"],
        "list":{
            "1": {"name": "rocketmq01", "ip": "172.16.1.1", "port": "22", "user": "root"},
            "2": {"name": "rocketmq02", "ip": "172.16.1.2", "port": "22", "user": "root"},
        }
    },
}

id_key = "~/.ssh/id_rsa"


def server_list_print(sl):
    for s in sl:
        t = "{0:<2}--> [ {1:<10}:\t{2:<} ]"
        if "name" in sl[s] and "ip" in sl[s]:
            print(t.format(s, sl[s]["name"], sl[s]["ip"]))

def go_server(server_list):
    while 1:
        print("当前群组：%s\n" %sys.argv[1])
        server_list_print(server_list["list"])
        server_id = input("输入序号和回车进入服务器，按q和回车则退出\n")
        if server_id == "q":
            break
        try:
            s = server_list["list"][server_id]
            command = ""
            if "port" in s:
                command = "ssh -o stricthostkeychecking=no -i " + id_key + " " + s["user"] + "@" + s["ip"] + " -p " + s["port"]
            ms = os.system(command)
            print(ms)
        except KeyError:
            continue

def check_user_permissions(group_name):
    real_user = ""
    if os.getenv("SUDO_USER") == None:
        real_user = os.getlogin()
    else:
        real_user = os.getenv("SUDO_USER")

    if real_user in ser[group_name]["can_use_user"]:
        # print("当前用户：%s" %(real_user))
        return True
    else:
        print("当前用户（%s）无登录主机组（%s）的权限，请使用sudo或检查用户权限。" %(real_user, group_name))
        exit(128)


def go():
    try:
        if len(sys.argv) == 0:
            raise KeyError
        else:
            check_user_permissions(sys.argv[1])
            go_server(ser[sys.argv[1]])
    except (KeyError,IndexError):
        g_list = list(ser.keys())
        str = ""
        if len(g_list) > 0:
            str = str + g_list[0]
        if len(g_list) > 1:
            for i in g_list[1:]:
                str = str + " || " + i
        print(str)


if __name__ == "__main__":
    go()