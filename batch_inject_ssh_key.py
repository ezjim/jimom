#!/usr/bin/env python
# -*- coding: utf-8 -*-


from sys import argv
import commands
import re


__author__ = 'James Iter'
__date__ = '2018/6/7'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2018 by James Iter.'


password = 'Tang3zang'
reg_ip = '^((?:(?:25[0-5]|2[0-4]\d|((1\d{2})|([1-9]?\d)))\.){3}(?:25[0-5]|2[0-4]\d|((1\d{2})|([1-9]?\d))))$'
pattern_ip = re.compile(reg_ip)


with open(argv[1], 'r') as f:
    for host in f:
        host = host.strip()

        try:
            if pattern_ip.match(host) is None:
                continue

            cmd = ' '.join(['/tmp/inject_ssh_key.exp', host, password])
            exit_status, output = commands.getstatusoutput(cmd)

            print str(output)
            print exit_status

        except Exception as e:
            print e.message

