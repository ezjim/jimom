#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
export PYPI='https://mirrors.aliyun.com/pypi/simple/'
pip install -i ${PYPI}
"""


import sys
from smtplib import SMTP
from email.mime.text import MIMEText


__author__ = 'James Iter'
__date__ = '2019-06-07'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2019 by James Iter.'


smtp_server = 'smtp.partner.outlook.cn'
smtp_port = 587
smtp_user = 'alarm@dophy.cn'
smtp_pswd = 'Dophy@2019'
mail_from = 'alarm@dophy.cn'


def incept_args():
    _args = {
        'mail_receivers': 'isoctech@163.com',
        'mail_title': u'报警邮件',
        'mail_message': '',
        'source_ip': sys.argv[1],
        'destination_ip': sys.argv[2],
        'event_name': sys.argv[3],
        'log_source': sys.argv[4]
    }

    with open('./send_mail.debug', 'a') as f:
        f.write(str(sys.argv) + '\n')

    return _args


def send_mail(mail_receivers, mail_title, mail_message, source_ip, destination_ip, event_name, log_source):

    mail_message += '<br>source_ip: ' + source_ip
    mail_message += '<br>destination_ip: ' + destination_ip
    mail_message += '<br>event_name: ' + event_name
    mail_message += '<br>log_source: ' + log_source

    msg_pkg = MIMEText(mail_message, 'html', 'utf8')
    msg_pkg['Subject'] = mail_title
    msg_pkg['From'] = mail_from
    msg_pkg['To'] = mail_receivers
    msg_pkg['CC'] = smtp_user

    s = SMTP(smtp_server, smtp_port, timeout=5)
    s.ehlo()
    s.starttls()
    s.login(smtp_user, smtp_pswd)
    s.sendmail(mail_from, mail_receivers.split(','), msg_pkg.as_string())
    s.close()


args = incept_args()
send_mail(args['mail_receivers'], args['mail_title'], args['mail_message'], args['source_ip'], args['destination_ip'],
          args['event_name'], args['log_source'])

