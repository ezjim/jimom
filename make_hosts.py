#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import getopt

__author__ = 'James Iter'
__date__ = '16/7/27'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2016 by James Iter.'


def usage():
    print "Usage:%s [-h|-i|-p|-d|-s|-l] [--help|--ip|--prefix|--domain|--start_at|--length]" % sys.argv[0]


def main():
    opts = None
    try:
        opts, args = getopt.getopt(sys.argv[1:], 'hi:p:d:s:l:',
                                   ['help', 'ip=', 'prefix=', 'domain=', 'start_at=', 'length='])
    except getopt.GetoptError as e:
        print str(e)
        usage()
        exit(e.message.__len__())

    ip = '192.168.0'
    prefix = '1v01'
    domain = 'iit.im'
    start_at = 1
    length = 10

    for k, v in opts:
        if k in ("-h", "--help"):
            usage()
            exit()
        elif k in ("-i", "--ip"):
            ip = v
        elif k in ("-p", "--prefix"):
            prefix = v
        elif k in ("-d", "--domain"):
            domain = v
        elif k in ("-s", "--start_at"):
            start_at = int(v)
        elif k in ("-l", "--length"):
            length = int(v)
        else:
            print "unhandled option"

    for i in range(start_at, start_at + length):
        i_str = i.__str__()
        full_ip = '.'.join([ip, i_str])
        hostname = '.'.join([prefix + i_str, domain])
        print '\t'.join([full_ip, hostname])


if '__main__' == __name__:
    main()

