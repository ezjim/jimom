#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
返回列表中，两个数相加等于目标值的索引。
"""


import traceback


__author__ = 'James Iter'
__date__ = '2017/9/14'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2017 by James Iter.'


class Solution(object):

    @staticmethod
    def two_sum(nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """
        z_num = None
        z_nums = list()
        e_num = None
        low_nums = list()
        high_nums = list()
        half = target / 2
        left_half = None
        right_half = None

        for i, num in enumerate(nums):
            if num == target:
                if num == 0:
                    z_num = i
                    z_nums.append(i)

                e_num = i

            elif num > half:
                high_nums.append(i)

            elif num == 0:
                z_num = i
                z_nums.append(i)

            elif num == half:
                if left_half is None:
                    left_half = i
                else:
                    right_half = i

            else:
                low_nums.append(i)

        if target == 0 and z_nums.__len__() > 1:
            return [z_nums[0], z_nums[-1]]

        if z_num is not None and e_num is not None:
            return [z_num, e_num]

        if left_half is not None and right_half is not None:
            return [left_half, right_half]

        # return low_nums, high_nums, half
        for l_num in low_nums:
            for h_num in high_nums:
                if target == nums[l_num] + nums[h_num]:
                    return [l_num, h_num]

        return False


if __name__ == '__main__':

    try:
        nums = [9, -11, 2, 45, -2, -3, 4, 3, 90, 22, 11, 90, 63, 45]
        target = 7
        print Solution.two_sum(nums=nums, target=target)

    except:
        print traceback.format_exc()

