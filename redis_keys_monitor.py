#!/usr/bin/env python
# -*- coding: utf-8 -*-


from redis.sentinel import Sentinel
import jimit as ji


__author__ = 'James Iter'
__date__ = '2019/12/4'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2019 by James Iter.'


redis_host = ''
redis_port = 18379
redis_dbid = 0
redis_password = 'acfc7ce6a937fefa9510'


sentinel = Sentinel([('10.24.22.140', redis_port), ('10.24.22.141', redis_port), ('10.24.22.142', redis_port)],
                    socket_timeout=0.1)
master = sentinel.master_for('redisMaster', socket_timeout=0.1, password=redis_password)

ret = master.keys('spring*')

with open('/opt/scripts/redis_keys_monitor.log', 'a') as f:
    f.writelines(ji.JITime.now_date_time() + ' : ' + str(ret.__len__()) + '\n')

