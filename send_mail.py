#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
export PYPI='https://mirrors.aliyun.com/pypi/simple/'
pip install -i ${PYPI}
"""


import getopt
import sys
from smtplib import SMTP
from email.mime.text import MIMEText


__author__ = 'James Iter'
__date__ = '2019-06-07'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2019 by James Iter.'


smtp_server = 'smtp.163.com'
smtp_port = 25
smtp_user = 'isoctech@163.com'
smtp_pswd = 'f4eFyxdb'
mail_from = 'isoctech@163.com'


def incept_args():
    _args = {
        'mail_receivers': None,
        'mail_title': None,
        'mail_message': '',
        'rule_name': '',
        'source_ip': '',
        'destination_ip': '',
        'event_name': ''
    }

    def usage():
        print "Usage:%s [-r] [--mail_receivers]" % sys.argv[0]
        print "-r --mail_receivers."
        print "-t --mail_title."
        print "-m --mail_message."
        print "-p --rule_name. p for policy, like rule."
        print "-s --source_ip."
        print "-d --destination_ip."
        print "-e --event_name."

    opts = None
    try:
        opts, _ = getopt.getopt(sys.argv[1:], 'hr:t:m:p:s:d:e:',
                                ['help', 'mail_receivers=', 'mail_title=', 'mail_message=', 'rule_name=', 'source_ip=',
                                 'destination_ip=', 'event_name='])

        print opts

    except getopt.GetoptError as e:
        print str(e)
        usage()
        exit(e.message.__len__())

    for k, v in opts:
        if k in ('-h', '--help'):
            usage()
            exit()

        elif k in ('-r', '--mail_receivers'):
            _args['mail_receivers'] = v

        elif k in ('-t', '--mail_title'):
            _args['mail_title'] = v

        elif k in ('-m', '--mail_message'):
            _args['mail_message'] = v

        elif k in ('-p', '--rule_name'):
            _args['rule_name'] = v

        elif k in ('-s', '--source_ip'):
            _args['source_ip'] = v

        elif k in ('-d', '--destination_ip'):
            _args['destination_ip'] = v

        elif k in ('-e', '--event_name'):
            _args['event_name'] = v

        else:
            print 'unhandled option'

    if 'mail_receivers' not in _args or _args['mail_receivers'] is None:
        print 'Must specify the -r(mail_receivers) arguments.'
        usage()
        exit(1)

    if 'mail_title' not in _args or _args['mail_title'] is None:
        print 'Must specify the -t(mail_title) arguments.'
        usage()
        exit(1)

    return _args


def send_mail(mail_receivers, mail_title, mail_message, source_ip, destination_ip, event_name, rule_name):

    mail_message += '<br>source_ip: ' + source_ip
    mail_message += '<br>destination_ip: ' + destination_ip
    mail_message += '<br>event_name: ' + event_name
    mail_message += '<br>rule_name: ' + rule_name

    msg_pkg = MIMEText(mail_message, 'html', 'utf8')
    msg_pkg['Subject'] = mail_title
    msg_pkg['From'] = mail_from
    msg_pkg['To'] = mail_receivers

    s = SMTP(smtp_server, smtp_port, timeout=5)
    # s.starttls()
    s.login(smtp_user, smtp_pswd)
    s.sendmail(mail_from, mail_receivers.split(','), msg_pkg.as_string())
    s.close()


args = incept_args()
send_mail(args['mail_receivers'], args['mail_title'], args['mail_message'], args['source_ip'], args['destination_ip'],
          args['event_name'], args['rule_name'])
