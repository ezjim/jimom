#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
export PYPI='https://mirrors.aliyun.com/pypi/simple/'
pip install -i ${PYPI} cx_Oracle

chroot /opt/qradar/bin/ca_jail/

wget --no-check-certificate https://files.pythonhosted.org/packages/5c/e0/be401c003291b56efc55aeba6a80ab790d3d4cece2778288d65323009420/pip-19.1.1-py2.py3-none-any.whl#sha256=993134f0475471b91452ca029d4390dc8f298ac63a712814f101cd1b6db46676
wget --no-check-certificate https://files.pythonhosted.org/packages/d7/71/517e598564f93e32c75573c9ab19253f2b6d6f59d1f6db208a8818bdbd24/cx_Oracle-7.1.3-cp27-cp27mu-manylinux1_x86_64.whl

python pip-19.1.1-py2.py3-none-any.whl/pip install --no-index pip-19.1.1-py2.py3-none-any.whl
pip install cx_Oracle-7.1.3-cp27-cp27mu-manylinux1_x86_64.whl

http://yum.oracle.com/repo/OracleLinux/OL7/oracle/instantclient/x86_64/index.html
wget --no-check-certificate http://yum.oracle.com/repo/OracleLinux/OL7/oracle/instantclient/x86_64/getPackage/oracle-instantclient19.3-basiclite-19.3.0.0.0-1.x86_64.rpm
yum localinstall oracle-instantclient19.3-basiclite-19.3.0.0.0-1.x86_64.rpm
"""


import os
import sys
import cx_Oracle


__author__ = 'James Iter'
__date__ = '2019-06-07'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2019 by James Iter.'


db_user = ''
db_password = ''
db_ip = '127.0.0.1'
db_port = 1521
db_name = ''
table_name = ''


os.environ['NLS_LANG'] = 'SIMPLIFIED CHINESE_CHINA.UTF8'


def incept_args():
    _args = {
        'source_ip': sys.argv[1],
        'destination_ip': sys.argv[2],
        'event_name': sys.argv[3],
        'log_source': sys.argv[4]
    }

    with open('./oracle_insert.debug', 'a') as f:
        f.write(str(sys.argv) + '\n')

    return _args


def insert_value(source_ip, destination_ip, event_name, log_source):
    db = cx_Oracle.connect(db_user, db_password, db_ip + ':' + db_port.__str__() + '/' + db_name)
    cur = db.cursor()
    statement = "insert into " + table_name + \
                "(source_ip, destination_ip, event_name, log_source) values (:1, :2, :3, :4)"
    cur.execute(statement, (source_ip, destination_ip, event_name, log_source))

    db.commit()
    cur.close()


args = incept_args()
insert_value(source_ip=args['source_ip'], destination_ip=args['destination_ip'], event_name=args['event_name'],
             log_source=args['log_source'])

