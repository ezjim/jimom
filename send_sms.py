#!/usr/bin/env python
# -*- coding: utf-8 -*-


import sys
import hashlib
import time
import json
import requests


__author__ = 'James Iter'
__date__ = '2021/8/13'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2021 by James Iter.'


gateway = 'http://sms.sinowel.com/zfrs/smsservice/json/SendSMS'
uid = '16699'
pwd = 'ZFrs_210713'
ts = str(int(time.time() * 1000))

phone = sys.argv[1]
msg = sys.argv[2]


def sign(_pwd, _ts):
    return hashlib.md5(uid + hashlib.md5(_pwd).hexdigest() + _ts).hexdigest()


def send_sms(_phone, _msg):
    payload = {
        "userId": uid,
        "sign": sign(_pwd=pwd, _ts=ts),
        "timestamp": ts,
        "messageList": [
            {
                "phone": _phone,
                "message": _msg
            }
        ]
    }

    headers = {'content-type': 'application/json'}
    return requests.post(gateway, data=json.dumps(payload), headers=headers)


r = send_sms(_phone=phone, _msg=msg)
print(r.content)
