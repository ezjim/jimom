#!/usr/bin/env python
# -*- coding: utf-8 -*-


from fabric.api import *
import jimit as ji


__author__ = 'James Iter'
__date__ = '2016/9/23'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2016 by James Iter.'


# pip install fabric -i http://pypi.douban.com/simple
# Example command "fab -f fabric/exampler1.py install_tool -P"

NET_10k = '10.10.0'

NET_1v00 = '10.1.16'
NET_1v01 = '10.1.17'
NET_1v02 = '10.1.18'
NET_1v03 = '10.1.19'
NET_1v04 = '10.1.20'
NET_1v05 = '10.1.21'
NET_1v06 = '10.1.22'

NET_10v01 = '10.10.1'
NET_10v02 = '10.10.2'

env.user = 'root'
env.key_filename = '~/.ssh/superman.id_rsa'
env.roledefs = {
    '1k': ['10.1.17.254', '10.1.18.254', '10.1.19.254', '10.1.20.254', '10.1.21.254', '10.1.22.254'],
    '10k': [],
    '1v01': [],
    '1v02': [],
    '1v03': [],
    '1v04': [],
    '1v05': [],
    '1v06': [],
    '10v01': [],
    '10v02': []
}


for i in range(1, 2 + 1):
    env.roledefs['10k'].append(NET_10k + '.' + str(i))

for i in range(1, 25 + 1):
    if i in [3, 4, 8, 9, 12, 13, 14, 15, 16, 17, 18, 19, 23, 25]:
        continue
    env.roledefs['1v01'].append(NET_1v01 + '.' + str(i))

for i in range(1, 7 + 1):
    env.roledefs['1v02'].append(NET_1v02 + '.' + str(i))

for i in range(1, 41 + 1):
    if i in [3, 23, 24, 25, 26, 27, 40]:
        continue
    env.roledefs['1v03'].append(NET_1v03 + '.' + str(i))

for i in range(1, 26 + 1):
    env.roledefs['1v04'].append(NET_1v04 + '.' + str(i))

for i in range(1, 37 + 1):
    if i in [6, 15, 26, 32]:
        continue
    env.roledefs['1v05'].append(NET_1v05 + '.' + str(i))

for i in range(1, 65 + 1):
    if i in [14, 18]:
        continue
    env.roledefs['1v06'].append(NET_1v06 + '.' + str(i))

for i in range(9, 9 + 1):
    env.roledefs['10v01'].append(NET_10v01 + '.' + str(i))

for i in range(17, 17 + 1):
    env.roledefs['10v02'].append(NET_10v02 + '.' + str(i))


def write_line(file_path, content):
    with open(file_path, 'a') as f:
        f.write(content)


def centos72_zabbix_agent():
    run('wget http://r.jkser.com/Linux/CentOS-Base.repo.aliyun -O /etc/yum.repos.d/CentOS-Base.repo')
    # run('yum clean all')
    run('yum remove zabbix-agent -y')
    run('yum install zabbix22-agent -y')
    run('sed -i "s@^Server=.*@Server=10.1.16.6, 10.1.17.6@" /etc/zabbix_agentd.conf')
    run('sed -i "s@^.\{0,1\}ServerActive=.*@ServerActive=10.1.17.6@" /etc/zabbix_agentd.conf')
    run('sed -i "s@^Hostname=.*@Hostname=`hostname`@" /etc/zabbix_agentd.conf')
    run('systemctl enable zabbix-agent')
    run('systemctl restart zabbix-agent')


def centos72_append_zabbix_disk_performance():
    run('rm -rf /etc/zabbix_agentd.conf.d')
    run('mkdir /etc/zabbix_agentd.conf.d')
    run('wget http://r.jkser.com/zabbix/userparameter_diskstats.conf -O '
        '/etc/zabbix_agentd.conf.d/userparameter_diskstats.conf')
    run('wget http://r.jkser.com/zabbix/lld-disks.py -O /usr/local/bin/lld-disks.py')
    run('chmod +x /usr/local/bin/lld-disks.py')
    run('sed -i "s@^# Include=/etc/zabbix_agentd.conf.d/@Include=/etc/zabbix_agentd.conf.d/@" /etc/zabbix_agentd.conf')
    run('systemctl restart zabbix-agent')


def centos72_zabbix_replace_server_ip():
    run('sed -i "s@10.10.0.6@10.1.16.6@" /etc/zabbix_agentd.conf')
    run('sed -i "s@10.10.1.6@10.1.17.6@" /etc/zabbix_agentd.conf')
    run('sed -i "s@^# ServerActive=127.0.0.1@ServerActive=10.1.17.6@" /etc/zabbix_agentd.conf')
    run('systemctl restart zabbix-agent')


def centos_new_user(user, password):
    run('useradd -m {user}'.format(user=user))
    run('echo "{user}:{password}" | chpasswd'.format(user=user, password=password))


def update_password(user, password):
    run('echo "{user}:{password}" | chpasswd'.format(user=user, password=password))


def gentoo_zabbix_agent():
    run('USE="agent" emerge zabbix')
    run('sed -i "s@^Server=.*@Server=10.1.16.6, 10.1.17.6@" /etc/zabbix/zabbix_agentd.conf')
    run('sed -i "s@^.\{0,2\}ServerActive=.*@ServerActive=10.1.17.6@" /etc/zabbix/zabbix_agentd.conf')
    run('sed -i "s@^Hostname=.*@Hostname=`hostname`@" /etc/zabbix/zabbix_agentd.conf')
    run('/etc/init.d/zabbix-agentd restart')
    run('rc-update add zabbix-agentd')


def centos72_install_tool():
    run('yum install htop -y')


def centos72_change_dns():
    run('sed -i "s@10.10.0.1@10.1.17.1@" /etc/sysconfig/network-scripts/ifcfg-eth0')
    run('sed -i "s@10.10.0.1@10.1.17.1@" /etc/resolv.conf')
    run('echo "nameserver 223.5.5.5" >> /etc/resolv.conf')


def pint_internet():
    run('ping -w 2 -c 1 www.xd.com')


def clear_ssh():
    run("sed -i 's$ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDfDHcxxq5Yjba8LdculoCvzKdwCOC3Kw279mnlTh5xcTZ3jfS1clzhdWUOHEsjUfQXIpMuegjyX1Y0i+pDLARYdaI1w/SVPeZujY7HtrFp9SDZrZD+GKAlCusp2aatyfPQqmUgpctz0smvzY4YlmvuU+XtVpZi+Pcs/10+78xmjc44KjgDUx0TWsDV354Nv5sJ3yoNf+IYzIjhbx01Yz2L7aldBlVQa4dFT175medjmpBkMK/fMIAJ6Au9DhcR4ahCStMeZFwxKe3w/pyLcmqTjXikeKeHzyto/+cryUF/3C2ZaxUS6+QsALJMWI/pUHDtTfbjXO4pl2pIAAVk85c5.*$$g' /root/.ssh/authorized_keys")


def clear_history():
    run('rm -f .bash_history; rm -f history')


@task()
@roles('1v05')
def get_os_type():
    run('grep -i name= /etc/os-release')


@task()
@roles('1v03')
def inject_zabbix_agent():
    centos72_zabbix_agent()
    centos72_append_zabbix_disk_performance()
    # gentoo_zabbix_agent()


@task()
@roles('1k')
def inject_new_user():
    user = 'monitor'
    pswd = ji.Common.generate_random_code(length=16, letter_form='mix', numeral=True)
    line = '\t'.join([env.host, user, pswd])
    write_line('./host_user_password.list', line + '\n')
    centos_new_user(user, pswd)


@task()
@roles('1v05', '1v06')
def install_tool():
    centos72_install_tool()


@task()
@roles('1v06')
def task_update_password():
    user = 'root'
    pswd = ji.Common.generate_random_code(length=16, letter_form='mix', numeral=True)
    line = '\t'.join([env.host, user, pswd])
    write_line('./host_user_password.list', line + '\n')
    update_password(user, pswd)


@task()
@roles('1v03')
def task_zabbix_replace_server_ip():
    centos72_zabbix_replace_server_ip()


@task()
@roles('1v06')
def task_change_dns():
    centos72_change_dns()


@task()
@roles('1k')
def internet_test():
    pint_internet()


@task()
@roles('1k')
def task_clear_ssh_history():
    clear_ssh()
    clear_history()

