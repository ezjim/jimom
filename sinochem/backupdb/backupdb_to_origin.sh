#!/bin/bash

# 异地备份脚本。凌晨03:05的时候将本地最后一个备份文件传到远程服务器
# 5 3 * * * cd /data/backups ; ./backupdb_to_origin.sh >> ./backupdb_to_origin.log

file_path=("/data/backups/db/196/" "/data/backups/db/210/" "/data/backups/db/1chemic_web/" "/data/backups/db/1chemic_oms/" "/data/backups/db/crm/" "/data/backups/db/tax/")
origin_path=("/opt/backups/db/196" "/opt/backups/db/210" "/opt/backups/db/1chemic_web/" "/opt/backups/db/1chemic_oms/" "/opt/backups/db/crm/" "/opt/backups/db/tax/")

date_now=$(date '+%Y-%m-%d-%H:%M:%S')

echo "开始今日异地备份"${date_now}

for((i=0; i < ${#file_path[@]}; i++))
do
    file=$(ls -tr ${file_path[${i}]} | tail -n 1)
    echo "开始备份文件"${file_path[${i}]}${file}
    scp -P 62200 ${file_path[${i}]}${file} centos@10.145.97.170:${origin_path[${i}]}
    if [ $? -eq 0 ] ; then
        echo "远程备份成功"
    else
        /opt/scripts/sendmsg/sendsms.py 15601603670 "数据库远程备份失败。"
        /opt/scripts/sendmsg/sendsms.py 17854222397 "数据库远程备份失败。"
        echo "远程备份失败,退出处理... "
        exit -1
    fi
done

echo -e "今日备份结束\n\n"
