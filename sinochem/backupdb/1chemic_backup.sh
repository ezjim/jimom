#!/bin/bash
# 20 10-23/2,2 * * *   /bin/bash /data/backup/backupdb.sh

dtTask=$(date '+%Y%m%d%H%M%S')
BIZ_FILENAME=1chemic_biz_$dtTask
OMS_FILENAME=1chemic_oms_$dtTask
DESDIR=/data/backups/files
echo "删除七天前的文件"
find  $DESDIR/ -type f -mtime +7 -exec rm -f {} \;
cd $DESDIR
mysqldump -u root -h 172.16.9.7 -u root -pjHYlKcAOKT6H7VYuv6xEaFWuYXEi9C3V. -R --add-drop-database -B sinochem_auction sinochem_common sinochem_finance sinochem_goods_order sinochem_member sinochem_pay sinochem_shop sinochem_v1_goods sinochem_v1_info sinochem_v1_interface sinochem_v1_logistic sinochem_v1_member sinochem_v1_mic sinochem_v1_order sinochem_v1_stock sinochem_v1_trade --routines --single-transaction --flush-logs --column-statistics=0 --master-data=2 > ${BIZ_FILENAME}.sql
mysqldump -u root -h 172.16.9.8 -u root -pjHYlKcAOKT6H7VYuv6xEaFWuYXEi9C3V. -R --add-drop-database -B sinochem_cmsdb sinochem_data sinochem_generator sinochem_omsdb --routines --single-transaction --flush-logs --column-statistics=0 --master-data=2 > ${OMS_FILENAME}.sql

tar Oc ${BIZ_FILENAME}.sql | pxz -T16 -cv - > $DESDIR/${BIZ_FILENAME}.sql.tar.xz
tar Oc ${OMS_FILENAME}.sql | pxz -T16 -cv - > $DESDIR/${OMS_FILENAME}.sql.tar.xz

rm -f ${BIZ_FILENAME}.sql
rm -f ${OMS_FILENAME}.sql

if [ $? -eq 0 ] ; then
        mv $DESDIR/${BIZ_FILENAME}.sql.tar.xz /data/backups/db/1chemic_web/
        mv $DESDIR/${OMS_FILENAME}.sql.tar.xz /data/backups/db/1chemic_oms/
        echo "[${dtTask}]归档MySQL数据库文件目录任务成功! "
else
        echo "[${dtTask}]归档MySQL数据库文件目录任务失败,退出处理... "
        exit -1
fi
