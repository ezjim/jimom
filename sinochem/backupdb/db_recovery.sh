# [root@db-recovery-prod ~]# crontab -l
# 0 2 15 * * /usr/bin/sh -x /opt/scripts/db_recovery.sh > /opt/scripts/logs/`date +"\%Y\%m\%d\%H\%M\%S"`.log 2>&1
# [root@db-recovery-prod ~]# cat /opt/scripts/db_recovery.sh
#!/bin/bash
# Author    : James Iter
# Made time : 20200513
# Function  : Database recovery test.


TICK=`date +"%Y%m%d%H%M%S"`
NAME="db_recovery_${TICK}"
DB_PSWD='dd7bdd059a00'
LOCAL_STORE_PATH="/opt/packages/"
LOCAL_DB_PATH="/data/mysql/${TICK}"
STORAGE_HOST="10.24.22.198"
ARCHIVE_PATH=`ssh ${STORAGE_HOST} "find /data/backups/db/196/ -type f -printf '%T+ %p\n' | sort -r | head -1 | awk {'print \\$2'}"`
ARCHIVE_FILE_NAME=`basename ${ARCHIVE_PATH}`

echo ${ARCHIVE_PATH}

mkdir -p ${LOCAL_STORE_PATH}
cd ${LOCAL_STORE_PATH}
scp ${STORAGE_HOST}:${ARCHIVE_PATH} ${LOCAL_STORE_PATH}
tar -xf ${ARCHIVE_FILE_NAME}
rm -f ${ARCHIVE_FILE_NAME}

SQL_PATH=`find ${LOCAL_STORE_PATH} -type f -name '*.sql' -printf '%T+ %p\n' | sort -r | head -1 | awk {'print \$2'}`

mkdir -p ${LOCAL_DB_PATH}
docker run --name ${NAME} -e MYSQL_ROOT_PASSWORD=${DB_PSWD} -v ${LOCAL_DB_PATH}:/var/lib/mysql -d mysql:8.0.20 --disable-log-bin

sleep 60

docker exec -i ${NAME} sh -c "exec mysql -u root -p${DB_PSWD}" < ${SQL_PATH}
docker exec -i ${NAME} sh -c "exec mysql -u root -p${DB_PSWD} -e 'SELECT table_schema AS \"Database\", SUM(data_length + index_length) / 1024 / 1024 / 1024 AS \"Size (GB)\" FROM information_schema.TABLES GROUP BY table_schema;'"
docker stop ${NAME}

rm -f ${SQL_PATH}
