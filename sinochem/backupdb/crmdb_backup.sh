#!/bin/bash

dtTask=$(date '+%Y%m%d%H%M%S')
FILENAME=crm_master_$dtTask
DESDIR=/data/backups/files
echo "删除七天前的文件"
find  $DESDIR/ -type f -mtime +7 -exec rm -f {} \;
mysqldump -u dba -pFT85fx4Ju7JHcH5T9X02\+ -h 172.16.9.9 -R --add-drop-database -B filedb crmdb --routines --single-transaction --flush-logs --master-data=2 > $DESDIR/${FILENAME}.sql

tar Oc $DESDIR/${FILENAME}.sql | pxz -T32 -cv - > $DESDIR/${FILENAME}.sql.tar.xz

rm -f $DESDIR/${FILENAME}.sql

if [ $? -eq 0 ] ; then
        echo "[${dtTask}]归档MySQL数据库文件目录任务成功! "
else
        /opt/sendsms.py 15601603670 "数据库备份失败。"
        echo "[${dtTask}]归档MySQL数据库文件目录任务失败,退出处理... "
        exit -1
fi
# scp -i /root/.ssh/wangqingchun $DESDIR/${FILENAME}.sql.tar.xz root@10.24.22.5:/data/backups/db/196/
mv $DESDIR/${FILENAME}.sql.tar.xz /data/backups/db/crm/
