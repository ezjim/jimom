#!/bin/bash

dtTask=$(date '+%Y%m%d%H%M%S')
FILENAME=data_skyline_$dtTask
DESDIR=/data/backups/files
mkdir -p ${DESDIR}

mysqldump -u root -pf82Z9c\!#2D9 -R --add-drop-database -B data_skyline --routines --single-transaction --flush-logs --master-data=2 > $DESDIR/${FILENAME}.sql

tar Oc $DESDIR/${FILENAME}.sql | pxz -T24 -cv - > $DESDIR/${FILENAME}.sql.tar.xz

rm -f $DESDIR/${FILENAME}.sql

if [ $? -eq 0 ] ; then
        echo "[${dtTask}]归档MySQL数据库文件目录任务成功! "
else
        echo "[${dtTask}]归档MySQL数据库文件目录任务失败,退出处理... "
        exit -1
fi


mv $DESDIR/${FILENAME}.sql.tar.xz /data/backups/db/198/
