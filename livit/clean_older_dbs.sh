#!/bin/bash

for db_path in `find /data/backups/db -ctime +60`;
do
    if [[ ${db_path: -17:2} != '02' ]]; then
        echo ${db_path}
        /usr/bin/rm -f ${db_path}
    fi
done
