#!/bin/bash
# Author    : James Iter
# Made time : 20221202
# Function  : Database recovery test.

BASE_PATH="/data/backups/db/"
DBS_NAMES=`ls ${BASE_PATH}`
WORK_SPACE="/opt/db_recovery_workspace/"
TICK=`date +"%Y%m%d%H%M%S"`
THE_MONTH_DATE=`date +"%Y%m"`
DB_PSWD='dd7bdd059a00'
mkdir -p ${WORK_SPACE}
cd ${WORK_SPACE}
ALREADY_DBS=`docker ps -a`
echo "开始数据恢复测试 $(date +"%Y%m%d%H%M%S")"
for db_name in ${DBS_NAMES}
do
    LOCAL_DB_PATH="/data/mysql/${db_name}/${TICK}"
    db_path_base=${BASE_PATH}${db_name}
    the_file=$(ls -tr ${db_path_base} | tail -n 1)
    file_path=${db_path_base}/${the_file}
    instance_name=${the_file%%.*}_${TICK}
    instance_name_with_month_date=${the_file%_*}_${THE_MONTH_DATE}
    if [[ ${#the_file} -eq 0 ]]; then
        continue
    fi
    if echo ${ALREADY_DBS} | grep -q ${instance_name_with_month_date}; then
        continue
    fi
    if [[ -f ${file_path} ]]; then
        echo "开始复制文件 ${file_path} $(date +"%Y%m%d%H%M%S")"
        cp ${file_path} .
        echo "开始解压 $(date +"%Y%m%d%H%M%S")"
        tar -xf ${the_file}
        /usr/bin/rm -f ${the_file}
        SQL_PATH=`find ${WORK_SPACE} -type f -name '*.sql' -printf '%T+ %p\n' | sort -r | head -1 | awk {'print \$2'}`
        mkdir -p ${LOCAL_DB_PATH}
        echo "开始启动验证环境 $(date +"%Y%m%d%H%M%S")"
        if [[ ${instance_name} =~ "ats" ]]; then
            docker run --name ${instance_name} -e MYSQL_ROOT_PASSWORD=${DB_PSWD} -p 3306:3306 -v ${LOCAL_DB_PATH}:/var/lib/mysql -d mysql:8.0.26 --disable-log-bin --max-allowed-packet=1G
        else
            docker run --name ${instance_name} -e MYSQL_ROOT_PASSWORD=${DB_PSWD} -p 3306:3306 -v ${LOCAL_DB_PATH}:/var/lib/mysql -d mysql:5.7.33 --disable-log-bin --max-allowed-packet=1G
        fi
        sleep 60
        echo "开始恢复数据 $(date +"%Y%m%d%H%M%S")"
        docker exec -i ${instance_name} sh -c "exec mysql -u root -p${DB_PSWD} --force" < ${SQL_PATH}
        docker exec -i ${instance_name} sh -c "exec mysql -u root -p${DB_PSWD} -e 'SELECT table_schema AS \"Database\", SUM(data_length + index_length) / 1024 / 1024 / 1024 AS \"Size (GB)\" FROM information_schema.TABLES GROUP BY table_schema;'"
        docker stop ${instance_name}
        echo "数据恢复完成 $(date +"%Y%m%d%H%M%S")"
    rm -f ${SQL_PATH}
    fi
done
echo "数据恢复测试结束 $(date +"%Y%m%d%H%M%S")"
