#!/bin/bash

# 异地备份摄取脚本。凌晨 04:05 的时候从远程服务器获取最新文件数据
# 5 4 * * * /opt/scripts/fetch_backup_files.sh >> /data/backups/logs/fetch_backup_files.log

REMOTE_IP="172.20.10.2"
BASE_PATH="/data/backups/files/"
FILE_NAMES=`ssh ${REMOTE_IP} "ls ${BASE_PATH}"`
DATETIME=$(date '+%Y-%m-%d-%H:%M:%S')
MOBILES="15601603670 15601603670 15802108237"

function alarmTo() {
    /opt/scripts/sendsms.py ${1} "文件数据 ${2} 远程备份失败。"
}

function doFetch() {
    echo "开始异地备份 $(date +"%Y%m%d%H%M%S")"

    for filename in ${FILE_NAMES}
    do
        file_path_base=${BASE_PATH}${filename}

        echo "开始备份文件 $(date +"%Y%m%d%H%M%S") "${file_path_base}
        mkdir -p ${file_path_base}
        rsync -av -e "ssh -p 22" root@${REMOTE_IP}:${file_path_base}/* ${file_path_base}/
        if [ $? -eq 0 ] ; then
            echo "备份成功 $(date +"%Y%m%d%H%M%S") "
        else
            for mobile in ${MOBILES}
            do
                alarmTo ${mobile} ${filename}
            done
            echo "${filename} 备份失败,退出处理... "
            exit -1
        fi
    done

    echo -e "今日备份结束 $(date +"%Y%m%d%H%M%S")\n\n"
}

doFetch
