#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
参考链接:
    http://pythonhosted.org/watchdog/quickstart.html#a-simple-example
    http://pythonhosted.org/watchdog/api.html#watchdog.events.FileSystemEventHandler

pip install watchdog
"""


import sys
import time
import logging
import os
from logging.handlers import TimedRotatingFileHandler
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

# import pydevd
# pydevd.settrace('180.168.96.38', port=51234, stdoutToServer=True, stderrToServer=True)


reload(sys)
sys.setdefaultencoding('utf8')

__author__ = 'James Iter'
__date__ = '2016/11/1'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2016 by James Iter.'


process_title = 'monitor_dir_by_watchdog'
log_file_path = 'c:/log/' + process_title + '.log'

log_dir = os.path.dirname(log_file_path)
if not os.path.isdir(log_dir):
    os.makedirs(log_dir, 0755)


class Init(object):
    @staticmethod
    def init_logger():
        _logger = logging.getLogger(process_title)

        _logger.setLevel(logging.DEBUG)

        fh = TimedRotatingFileHandler(log_file_path, when='W6', interval=1, backupCount=7)
        formatter = logging.Formatter(
            '%(asctime)s - %(name)s - %(levelname)s - %(funcName)s - %(lineno)s - %(message)s')
        fh.setFormatter(formatter)
        _logger.addHandler(fh)
        return _logger


logger = Init.init_logger()


class EventHandler(FileSystemEventHandler):

    def on_created(self, event):
        print event.src_path + ' Created!'
        logger.info(event.src_path + ' Created!')

    def on_deleted(self, event):
        print event.src_path + ' Deleted!'
        logger.info(event.src_path + ' Deleted!')

    def on_modified(self, event):
        if not event.is_directory:
            print event.src_path + ' Modified!'
            logger.info(event.src_path + ' Modified!')


if __name__ == "__main__":
    path = sys.argv[1] if len(sys.argv) > 1 else '.'
    event_handler = EventHandler()
    observer = Observer()
    observer.schedule(event_handler, path.decode('gbk'), recursive=True)
    observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()

