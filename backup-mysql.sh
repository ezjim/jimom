#!/bin/bash
db_with="mysql/";
today=`date +%Y-%m-%d`;
now=`date +%H`;
ssh_user="root";                          # SSH username
ssh_port=22;                               # SSH port
ssh_iden="-i /home/www/.ssh/id_rsa"   #SSH private key
if [ ! -d $db_with ];then
    mkdir -p $db_with;
fi
cd $db_with;
if [ ! -d $today ];then
    mkdir -p $today;
fi
cd $today;
if [ ! -d $now ];then
        mkdir -p $now;
fi
cd $now

# mysqldump -u root -pjimpswd.com --add-drop-database --all-databases --routines --single-transaction --flush-logs --master-data=2 -h MySQL_A_02.177.com.tw > now_dbs.sql
mysqldump -u root -pjimpswd.com -R --add-drop-database -B 66style www_177_com_tw ad.midasgame.com.tw bbs.177.com.tw employees openx x.midasgame.com.tw xbk.midasgame.com.tw --routines --single-transaction --flush-logs --master-data=2 -h 192.168.100.26 > now_dbs.sql
tar Oc now_dbs.sql | pxz -T4 -cv - > now_dbs.sql.tar.xz
rm -f now_dbs.sql
