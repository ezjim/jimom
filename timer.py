#!/usr/bin/env python
# -*- coding: utf-8 -*-


import time


__author__ = 'James Iter'
__date__ = '2017/4/3'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2017 by James Iter.'


class Timer:
    def __init__(self):
        self.time = 0.0
        self.counter = 0
        self._start = False

    def start(self):
        # No.1
        # 开始写代码，请实现秒表启动时所需进行的操作
        if not self._start:
            self._start = True
            self.counter = time.time()

    def end(self):
        # No.2
        # 开始写代码，请实现秒表停止时所需进行的操作
        if self._start:
            self.time = time.time() - self.counter
            self._start = False

    def reset(self):
        # No.3
        # 开始写代码，请实现秒表重设时所需进行的操作
        self.time = 0.0

    # end_code

    @property
    def running(self):
        return self._start is not None

    def __enter__(self):
        self.start()
        return self

    def __exit__(self, *args):
        self.end()


if __name__ == '__main__':
    def count(n):
        while n > 0:
            n -= 1

    t = Timer()
    t.start()
    time.sleep(2)
    count(1000000)
    t.end()
    print(t.time)

    t.reset()

    t.start()
    time.sleep(5)
    count(1000000)
    t.end()
    print(t.time)
