#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '16/9/2'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2016 by James Iter.'


"""
    1   1   1   1   1   1
    6   5   4   3   2   1
    21  15  10  6   3   1
    56  35  20  10  4   1
    126 70  35  15  5   1
    252 126 56  21  6   1
"""


width = 10
sequence = map(lambda x: 1, range(width))


def self_processing(_sequence):
    next_left_num = 0
    new_sequence = list()
    for x in range(0, _sequence.__len__()):
        next_left_num += _sequence[x]
        new_sequence.append(next_left_num)

    return new_sequence

print '\t\t'.join(map(lambda x: str(x), sequence.__iter__()))
for i in range(1, width):
    sequence = self_processing(sequence)
    print '\t\t'.join(map(lambda x: str(x), sequence.__iter__()))
